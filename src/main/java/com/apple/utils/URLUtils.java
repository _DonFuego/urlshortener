package com.apple.utils;

/**
 * This is the URL utility that is used to shorten urls using a base-62 conversion.
 *
 * Created by tmatthews on 7/9/17.
 */
public class URLUtils {

    //Contains our elements for the converter to choose from
    private static final String ALPHABET = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    //Quick calculation of the base of characters
    private static final int BASE = ALPHABET.length();

    /**
     * URL Shortener using a basic bijective function via Base-62 conversion of an autoincremented identifier
     *
     * @param autoIncrementedID
     * @return String
     */
    public static String shorten(Long autoIncrementedID) throws Exception {
        if (null == autoIncrementedID || autoIncrementedID <= 0) {
            throw new Exception("Invalid Auto Incremented ID for URL Shortener!");
        }
        StringBuilder stringBuilder = new StringBuilder();
        while ( autoIncrementedID > 0 ) {
            stringBuilder.append( ALPHABET.charAt((int) (autoIncrementedID % BASE)) );
            autoIncrementedID /= BASE;
        }
        return stringBuilder.reverse().toString();
    }
}