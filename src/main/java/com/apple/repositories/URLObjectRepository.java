package com.apple.repositories;

import com.apple.models.URLObject;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * URLObjectRepository
 *
 * Defines all operations against the URLObject Table in h2 database.
 *
 * Created by tmatthews on 7/9/17.
 */
public interface URLObjectRepository extends CrudRepository<URLObject, Long> {
    /**
     * Finds a URL Object by the originally entered URL.
     *
     * @param original_url
     * @return  A list of url objects whose original url is an exact match with the given url.
     */
    @Query("SELECT u FROM URLObject u WHERE u.original_url = :original_url")
    List<URLObject> findByOriginalURL(@Param("original_url") String original_url);

    /**
     * Finds a URL Object by the shortened URL code.
     *
     * @param shortened_url
     * @return  A list of url objects whose original url is an exact match with the given url.
     */
    @Query("SELECT u FROM URLObject u WHERE u.shortened_url = :shortened_url")
    List<URLObject> findByShortenedURL(@Param("shortened_url") String shortened_url);

}
