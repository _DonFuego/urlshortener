package com.apple.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * URLObject Model Class
 *
 * Simple POJO for storing a shortened URL
 *
 * Created by tmatthews on 7/9/17.
 */
@Entity
public class URLObject {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(unique=true)
    @Pattern(regexp="(http(s)?://)?([\\w-]+\\.)+[\\w-]+(/[\\w- ;,./?%&=]*)?", message = "Please enter a valid URL")
    private String original_url;

    @Column(unique=true)
    private String shortened_url;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getOriginal_url() {
        return original_url;
    }

    public void setOriginal_url(String original_url) {
        this.original_url = original_url;
    }

    public String getShortened_url() {
        return shortened_url;
    }

    public void setShortened_url(String shortened_url) {
        this.shortened_url = shortened_url;
    }
}
