package com.apple.bootstrap;

import com.apple.models.URLObject;
import com.apple.repositories.URLObjectRepository;
import com.apple.utils.URLUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

@Component
public class URLObjectLoader implements ApplicationListener<ContextRefreshedEvent> {

    private URLObjectRepository urlObjectRepository;

    private Logger log = Logger.getLogger(URLObjectLoader.class);

    @Autowired
    public void setProductRepository(URLObjectRepository urlObjectRepository) {
        this.urlObjectRepository = urlObjectRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {

        URLObject shortenedURL = new URLObject();
        shortenedURL.setOriginal_url("http://www.apple.com");
        shortenedURL.setShortened_url("a");
        urlObjectRepository.save(shortenedURL);

        log.info("Saved URLObject - id: " + shortenedURL.getId());
    }
}
