package com.apple.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.io.IOException;

/**
 * BaseController
 *
 * A base controller to handle anything common to other controllers.
 *
 * Contains basic exception handler that listens for thrown Exceptions and shows an error page.
 * This obviously could have gotten more elaborate by creating custom exceptions and mappings
 * but for the purpose of this it seems to suffice.
 *
 * Created by tmatthews on 7/9/17.
 */
@Controller
public class BaseController {

    /**
     * Handler for any thrown Exception.class from controllers that extend this controller.  This
     * will grab the error message from the exception and show it on the error template page.
     *
     * @param e
     * @param model
     * @return String error template
     * @throws IOException
     */
    @ExceptionHandler
    private String handleException(Exception e, Model model) throws IOException {
        model.addAttribute("error", e.getMessage());
        return "error";
    }
}
