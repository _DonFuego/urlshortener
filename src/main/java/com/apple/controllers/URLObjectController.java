package com.apple.controllers;

import com.apple.models.URLObject;
import com.apple.repositories.URLObjectRepository;
import com.apple.utils.URLUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.List;

/**
 * This is the controller that enables all interactions with saving and retrieving
 * shortened URLs.
 *
 * Created by tmatthews on 7/9/17.
 */
@Controller
public class URLObjectController extends BaseController {

    private static Logger logger = LoggerFactory.getLogger(IndexController.class);

    //The data repository for storing/retrieving shortened URL's
    private URLObjectRepository urlObjectRepository;

    @Autowired
    public void setProductRepository(URLObjectRepository urlObjectRepository) {
        this.urlObjectRepository = urlObjectRepository;
    }

    /**
     * Shows a list of all shortened URL's.
     *
     * @param model
     * @return String
     */
    @RequestMapping(value = "/urlObjects", method = RequestMethod.GET)
    public String list(Model model) {
        model.addAttribute("urlObjects", urlObjectRepository.findAll());
        return "urlObjects";
    }

    /**
     * Route to facilitate creating a new shortened URL.
     *
     * @param model
     * @return String
     */
    @RequestMapping("urlObject/new")
    public String create(Model model) {
        model.addAttribute("urlObject", new URLObject());
        return "urlObjectForm";
    }

    /**
     * Saves the URL to be shortened and generates it's unique shortcode.
     * //TODO: I don't like that this operation performs two saves, but at least it gives us an auto-incremented id that I don't have to separately manage
     *
     * @param urlObject
     * @return String
     */
    @RequestMapping(value = "urlObject", method = RequestMethod.POST)
    public String save(@ModelAttribute("urlObject") @Valid URLObject urlObject, BindingResult bindingResult) throws Exception {
        if (bindingResult.hasErrors()) {
            logger.error("User entered invalid URL!");
            return "index";
        }

        //First, ensure that this URL isn't already stored.
        List<URLObject> urlObjectList = urlObjectRepository.findByOriginalURL(urlObject.getOriginal_url());

        if (null != urlObjectList && urlObjectList.isEmpty()) {
            logger.debug("This URL is unique within the current database.");
            try {
                //First save the URL so we can use it's autoincremented id to shorten the URL
                urlObjectRepository.save(urlObject);
                if (urlObject.getId() > 0) {
                    urlObject.setShortened_url(URLUtils.shorten(urlObject.getId()));
                    urlObjectRepository.save(urlObject);
                }
            } catch (Exception e) {
                logger.error("There was an error saving this URL! ", e);
                throw new Exception ("There was a problem saving this URL - contact customer support while you panic!");
            }
        } else {
            logger.debug("This URL has already been shortened!");
            return "redirect:/urlObject/" + urlObjectList.get(0).getId();
        }

        return "redirect:/urlObject/" + urlObject.getId();
    }

    /**
     * Shows the given shortened URL.
     *
     * @param id
     * @param model
     * @return String
     */
    @RequestMapping("urlObject/{id}")
    public String show(@PathVariable Long id, Model model){
        model.addAttribute("urlObject", urlObjectRepository.findOne(id));
        return "urlObjectShow";
    }

    /**
     * Allows you to edit the existing shortened URL.
     * @param id
     * @param model
     * @return String
     */
    @RequestMapping("urlObject/edit/{id}")
    public String update(@PathVariable Long id, Model model){
        model.addAttribute("urlObject", urlObjectRepository.findOne(id));
        return "urlObjectForm";
    }

    /**
     * Deletes the shortened URL in the database
     * @param id
     * @return String
     */
    @RequestMapping("urlObject/delete/{id}")
    public String delete(@PathVariable Long id){
        urlObjectRepository.delete(id);
        return "redirect:/urlObjects";
    }

}
