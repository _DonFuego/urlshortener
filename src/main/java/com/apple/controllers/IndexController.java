package com.apple.controllers;

import com.apple.models.URLObject;
import com.apple.repositories.URLObjectRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * IndexController
 *
 * Shows the initial blank form for a user to enter a URL in which to shorten.
 * Given a unique shortened URL id - redirects the user to that page.
 *
 * @author tmatthews
 */
@Controller
public class IndexController extends BaseController {

    private static Logger logger = LoggerFactory.getLogger(IndexController.class);

    //The data repository for storing/retrieving shortened URL's
    private URLObjectRepository urlObjectRepository;

    @Autowired
    public void setProductRepository(URLObjectRepository urlObjectRepository) {
        this.urlObjectRepository = urlObjectRepository;
    }

    /**
     * Shows the initial index page with blank input form.
     *
     * @param model
     * @return the index thymeleaf template
     */
    @RequestMapping("/")
    String index(Model model) {
        model.addAttribute("urlObject", new URLObject());
        logger.debug("Empty url object created");
        return "index";
    }

    /**
     * Redirects the user to the correct shortened URL by the given ID.
     *
     * @param shortCode
     * @param model
     * @return redirect to the given stored page or show an error if the given ID is not found.
     */
    @RequestMapping("/{shortCode}")
    String index(@PathVariable String shortCode, Model model) throws Exception {
        if (null != shortCode) {
            logger.debug("Attempting to redirect to url based off of shortcode: " + shortCode);
            List<URLObject> urlObjectList = urlObjectRepository.findByShortenedURL(shortCode);
            if (null != urlObjectList && !urlObjectList.isEmpty()) {
                logger.debug("Found a shortened url, redirecting to: " + urlObjectList.get(0).getOriginal_url());
                return "redirect:" + urlObjectList.get(0).getOriginal_url();
            }
        }
        logger.error("Poop!  We didn't find a shortened URL for the input given!");
        throw new Exception("That shortened URL is not in our system.");
    }
}