package com.apple.configs;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Created by tmatthews on 7/9/17.
 */
@Configuration
@EnableAutoConfiguration
@EntityScan(basePackages = {"com.apple.models"})
@EnableJpaRepositories(basePackages = {"com.apple.repositories"})
@EnableTransactionManagement
public class RepositoryConfiguration {
}
