package com.apple;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * URLShortenerApplication
 *
 * This is a very basic spring boot application.
 *
 * @author tmatthews
 */
@SpringBootApplication
public class URLShortenerApplication {

    public static void main(String[] args) {
        SpringApplication.run(URLShortenerApplication.class, args);
    }

}
