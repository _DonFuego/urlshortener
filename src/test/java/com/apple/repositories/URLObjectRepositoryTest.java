package com.apple.repositories;

import com.apple.URLShortenerApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * Simple Repository test - since the URLObjectLoader has a event listener
 * on it listening for application start events...it will attempt to autowire the
 * URLObjectRepository and load data in the in-memory h2 database.  This will fail
 * if anything during the bootstrap process goes awry.
 *
 * //TODO: This can obviously be enhanced to run various queries to ensure data is loaded...etc.
 *
 * Created by tmatthews on 7/9/17.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = URLShortenerApplication.class)
@WebAppConfiguration
public class URLObjectRepositoryTest {
    @Test
    public void contextLoads() {
    }
}