package com.apple.utils;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * URLUtils Test Class
 *
 * Created by tmatthews on 7/9/17.
 */
@RunWith(SpringRunner.class)
public class URLUtilsTest {

    private static Logger logger = LoggerFactory.getLogger(URLUtilsTest.class);

    /**
     * This tests to ensure our shortener is creating the correct short codes.
     */
    @Test
    public void simpleURLShortenTest() {
        try {
            assertEquals(URLUtils.shorten(1l), "b");
            assertEquals(URLUtils.shorten(123l), "bZ");
            assertEquals(URLUtils.shorten(456787654l), "4UDuO");
            assertEquals(URLUtils.shorten(765432l), "dnhG");
            assertEquals(URLUtils.shorten(193745892674l), "dz3UbsO");
        } catch (Exception e) {
            assertEquals(e.getMessage(), "Invalid Auto Incremented ID for URL Shortener!");
        }
    }

    @Test
    public void badURLShortenTest() {
        try {
            URLUtils.shorten(-10l);
        } catch (Exception e) {
            assertEquals(e.getMessage(), "Invalid Auto Incremented ID for URL Shortener!");
        }
    }

    @Test
    public void badURLShortenNullTest() {
        try {
            URLUtils.shorten(null);
        } catch (Exception e) {
            assertEquals(e.getMessage(), "Invalid Auto Incremented ID for URL Shortener!");
        }
    }



}
