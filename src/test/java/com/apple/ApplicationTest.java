package com.apple;

import com.apple.models.URLObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Application Test
 *
 * @author tmatthews
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ApplicationTest {

    @Autowired
    private MockMvc mockMvc;

    /**
     * Tests that our application loads up and the index page shows.
     *
     * @throws Exception
     */
    @Test
    public void index() throws Exception {
        mockMvc.perform(get("/"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("URL Shortener Application")))
                .andExpect(view().name("index"))
                .andExpect(model().attribute("urlObject", hasProperty("id")))
                .andExpect(model().attribute("urlObject", hasProperty("original_url")))
                .andExpect(model().attribute("urlObject", hasProperty("shortened_url")));
    }

    /**
     * Tests that our application loads up and the index page shows.
     *
     * @throws Exception
     */
    @Test
    public void missingRedirectURL() throws Exception {
        mockMvc.perform(get("/bbb"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("That shortened URL is not in our system.")));
    }

    /**
     * Tests that our application loads up and the index page shows.
     *
     * @throws Exception
     */
    @Test
    public void submitNewURL() throws Exception {
        URLObject newURLObject = new URLObject();
        mockMvc.perform(post("/urlObject").param("urlObject.original_url", "http://www.cnn.com"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/urlObject/2"));
    }

    /**
     * Tests that given our default loaded shortened URL which is saved on bootstrap
     * correctly redirects to apple.com
     *
     * @throws Exception
     */
    @Test
    public void redirectToShortenedURL() throws Exception {
        mockMvc.perform(get("/a"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("http://www.apple.com"));
    }



}
