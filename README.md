# README #

Code Testing Assignment

### What is this repository for? ###

The assignment was given as follows:
URL Shortener

We'd like you to build a website that functions as a URL Shortener:

1. A user should be able to load the index page of your site and be presented with an input field where they can enter a URL.

1. Upon entering the URL, a "shortened" version of that url is created and shown to the user as a URL to the site you're building.

1. When visiting that "shortened" version of the URL, the user is redirected to the original URL.

1. Additionally, if a URL has already been shortened by the system, and it is entered a second time, the first shortened URL should be given back to the user.


For example, if I enter http://www.apple.com/iphone-7/ into the input field, and I'm running the app locally on port 9000, I'd expect to be given back a URL that looked something like http://localhost:9000/1. Then when I visit http://localhost:9000/1, I am redirected to http://www.apple.com/iphone-7/.

### How do I get set up? ###

* Prerequisites: java 1.8
* clone repository locally
* Run `./gradlew build` then `java -jar build/libs/urlShortener-0.1.0.jar`
* Go to `localhost:9000` in your browser to view application
* Go to `localhost:9000/console` to view the h2 database: JDBCUrl: jdbc:h2:mem:testdb, username: sa, password: [blank]

### Who do I talk to? ###

* Todd Matthews - matthews.todd@gmail.com